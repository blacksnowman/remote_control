#include "common.h"

long getMicrotime() {
	struct timeval currentTime;
	gettimeofday(&currentTime, NULL);
	return currentTime.tv_sec * (int)1e6 + currentTime.tv_usec;
}

int in_array(char *element, char **arr, int length) {
	int i=0;
	for(i=0; i < length; i++) {
		if(strcmp(element, arr[i]) == 0){
			return 0;
		}
	}
	return 1;
}

const char *get_filename_ext(const char *filename) {
    const char *dot = strrchr(filename, '.');
    if(!dot || dot == filename) return "";
    return dot + 1;
}

void print_r(char **arr, int length){
	int k=0;
	for(k=0; k < length; k++) {
		printf("\"%s\" \n", arr[k]);
	}
}

void addToList(char **list, char *element, int *length) {
	list[*length] = '\0';
	list[*length] = strdup(element);
	*length = *length + 1;
}

void split(char **arr, char *str, char *delimiter, int *len) {
	int i=0;
	char *buffer = strdup(str);
	char *token = strtok(buffer, delimiter);

	while ((token != NULL)&&(i <= MAX_SPLIT)) {
		//printf ("Token[%d] = %s\n",i,token);
		int cur_len = strlen(token);
		int alloc_mem_size = cur_len+1;
		arr[i] = malloc(alloc_mem_size*sizeof(char));
		memset(arr[i],0,alloc_mem_size);
		memcpy(arr[i], token, cur_len);
		token = strtok(NULL,delimiter);
 		i++;
 	}
 	
 	*len = i;
}

void cutString(char *new_str, char *str, char *prefix) {
		int j=0; 
		
		int prefix_len = strlen(prefix);
		int new_str_len = strlen(str)-prefix_len;
		int alloc_size =  new_str_len + 1;
		new_str = (char *) malloc(alloc_size*sizeof(char));
		memset(new_str,0,alloc_size);
		
		for(j=0;j<new_str_len; j++){
			new_str[j] = str[j+prefix_len];
		}
}

void clearList(char **list, int *length) {
	int k=0;
	
	if(*length > 0) {
		for(k=0; k < *length; k++) {
			list[k] = '\0';
		}
		*length = 0;
	}
}

int regmatch(const char *buffer, const char *tpl) {
	regex_t regex;
	int reti;

	reti = regcomp(&regex, tpl, REG_EXTENDED);
    if( reti ) { 
    	fprintf(stderr, "Could not compile regex\n"); 
    	return -1;
    }

	reti = regexec(&regex, buffer, 0, NULL, 0);
	if (!reti) {
	    return 0;
	} else {
		return -1;
	}
}

int regmatch_find(char *value, const char *buffer, const char *tpl) {
	regex_t regex;
	regmatch_t m[MAX_MATCH];

	int reti = regcomp(&regex, tpl, REG_EXTENDED|REG_NEWLINE);
    if( reti ) { 
    	fprintf(stderr, "Could not compile regex\n"); 
    	return -1;
    }

    const char *p = buffer;
    int j=0;
    while (1) {
    	int i = 0;
        int nomatch = regexec(&regex, p, MAX_MATCH, m, 0);
        //printf("\nnomatch = %d", nomatch);
        if (nomatch) {
        	//printf ("No more matches: %d\n", i);
       		return 1;
       	} else {
       		for (i=0; i < MAX_MATCH; i++) {
                int start;
	            int finish;
	            if (m[i].rm_so == -1) {
	                break;
	            }
	            start = m[i].rm_so + (p - buffer);
	            finish = m[i].rm_eo + (p - buffer);

	            if(i > 0) {
	            	int cur_len = (finish - start);
	            	int alloc_mem_size = cur_len + 1;
	            	if(i==1) {
	            		memcpy(value, (buffer + start), cur_len);
	            		return 0;
	            	}
	        	}
            }
            p += m[0].rm_eo;
       		j++; 
       	}
	}
}