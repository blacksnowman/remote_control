#include <errno.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <libgen.h>

#define MAX_MATCH 100
#define MAX_SPLIT 100

long getMicrotime();
int in_array(char *element, char **arr, int length);
void print_r(char **arr, int length);
void addToList(char **list, char *element, int *length);
void split(char **arr,char *str, char *delimiter, int *len);
void cutString(char *new_str, char *str, char *prefix);
void clearList(char **list, int *length);
int regmatch(const char *buffer, const char *tpl);
int regmatch_find(char *value, const char *buffer, const char *tpl);
const char *get_filename_ext(const char *filename);
