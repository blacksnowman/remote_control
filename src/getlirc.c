#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include "common.h"
#include <dirent.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include <arpa/inet.h> //inet_addr
#include "lirc/lirc_client.h"

#define MAX_LINES 50
#define STRING_BUFFER_SIZE 1024
#define MAX_CMD_LENGTH 512
#define DELAY  999000//1573000
#define MAX_BUTTON_DELAY 6500000
#define MPD_PORT 6600
#define MPD_ADDR "127.0.0.1"
#define SEND_BUFFER 1024
#define RCV_BUFFER 4096
#define MAX_SCAN_DEPTH 5
#define MUSIC_DIR "/var/lib/mpd/music"

//audio sources symlinks
#define INTERNAL "music"
#define USB "usb"
#define NAS "remote"

sig_atomic_t signaled = 0;

void my_handler(int param) {
	signaled = 1;
}

char *audio[1024];
int audio_count = 0;

char *cue[255];
int pl_count = 0;

/* current playlist id */
int current_playlist = 0;

/* time structure */
typedef struct {
	char hex[16]; //000000037ff07bf8
	int number;
	char keycode[10];
	char device[10];
}
TButton;

/* playlist structure */
typedef struct {
	int type; // 1 - playlist: m3u or CUE; 2 - directory with files
	char *path;
}
TPlaylist;

TPlaylist *playlists[1024];
int playlists_length = 0;

int sendCommandList2MDP(char **cmd, int length) 
{
	int sock, i;
    struct sockaddr_in server;
    char message[SEND_BUFFER] , server_reply[RCV_BUFFER];
    memset(message,0,SEND_BUFFER);
	memset(server_reply,0,RCV_BUFFER);
    //Create socket
    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock == -1) {
        printf("Could not create socket");
    }
   
    server.sin_addr.s_addr = inet_addr(MPD_ADDR);
    server.sin_family = AF_INET;
    server.sin_port = htons(MPD_PORT);
 
    //Connect to remote server
    if (connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0) {
        perror("connect failed. Error");
        return 1;
    }
     
	//Receive a reply from the server
	if( recv(sock , server_reply , RCV_BUFFER , 0) < 0) {
		puts("Socket receive failed");
		return 1;
	} else {
		memset(server_reply, 0, RCV_BUFFER);
	}
   
   	for(i = 0; i < length; i++) {
		sprintf(message, "%s\n", cmd[i]);
		//Send some data
		if( send(sock, message, strlen(message) , 0) < 0) {
			puts("Socket send failed");
			return 1;
		} else {
			//printf("Sended to server: %s", message);
			memset(message, 0, SEND_BUFFER);
		}
	}
	
	//Receive a reply from the server
	if( recv(sock , server_reply , RCV_BUFFER , 0) < 0) {
		puts("Socket receive failed");
		return 1;
	}
    
    close(sock);
    return 0;
}

int send2MPD(char *cmd) 
{
	int sock;
    struct sockaddr_in server;
    char message[SEND_BUFFER], server_reply[RCV_BUFFER];
    memset(message, 0, SEND_BUFFER);
	memset(server_reply, 0, RCV_BUFFER);
    //Create socket
    sock = socket(AF_INET , SOCK_STREAM , 0);

    if (sock == -1) {
        printf("Could not create socket");
    }
    //puts("Socket created");
     
    server.sin_addr.s_addr = inet_addr(MPD_ADDR);
    server.sin_family = AF_INET;
    server.sin_port = htons(MPD_PORT);
 
    //Connect to remote server
    if (connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0) {
        perror("connect failed. Error");
        return 1;
    }
     
	//Receive a reply from the server
	if( recv(sock , server_reply , RCV_BUFFER , 0) < 0) {
		puts("Socket receive failed");
		return 1;
	} else {
		memset(server_reply, 0, RCV_BUFFER);
	}
   
	sprintf(message, "%s\n", cmd);

	//Send some data
	if(send(sock, message, strlen(message), 0) < 0) {
		puts("Socket send failed");
		return 1;
	} else {
		memset(message, 0, SEND_BUFFER);
	}

	//Receive a reply from the server
	if( recv(sock, server_reply , RCV_BUFFER , 0) < 0) {
		puts("Socket receive failed");
		return 1;
	}
	 
    close(sock);
    return 0;
}

void parseCUE(char *directory, char *filename, char **audio, int *audio_count) 
{
 	FILE * fp;

    char line[255];
	char *line_pointer;
    size_t len = 0;
    ssize_t read;

	char full_path[STRING_BUFFER_SIZE];
	memset(full_path, 0, STRING_BUFFER_SIZE);
    sprintf(full_path, "%s/%s", MUSIC_DIR, filename);

    fp = fopen(full_path, "r");

    if (fp == NULL) {
    	printf("Reading file %s failed\n", full_path);
        exit(EXIT_FAILURE);
    }

    
    while (!feof (fp)) {
    	line_pointer = fgets(line, 255, fp);
        char value[128] = {'\0'};

        if(regmatch_find(value, line, "FILE \"([^\"]+)\"") == 0) {
        	memset(full_path, 0, STRING_BUFFER_SIZE);
			sprintf(full_path, "%s/%s", directory, value);
        	addToList(audio, full_path, audio_count);
        }
    }

    fclose(fp);
}

TPlaylist *createPlaylist(char type, char *path) 
{
	TPlaylist *p = malloc(sizeof(TPlaylist));
	p->type = type;
	p->path = strdup(path);

	return p;
}

void addToStructList(TPlaylist **list, TPlaylist *element, int *length) 
{
	list[*length] = malloc(sizeof(TPlaylist));
	list[*length]->type = element->type;
	list[*length]->path = strdup(element->path);
	*length = *length + 1;
}

void clearStructList(TPlaylist **list, int *length) 
{
	int k = 0;

	if(*length > 0) {
		for(k = 0; k < *length; k++) {
			list[k] = NULL;
		}

		*length = 0;
	}
}

int scanDirectoryMixed(char **cue_list, char *dir, int level, int *length) 
{
	DIR *dp;
	struct dirent *ep;
	
	char full_path[STRING_BUFFER_SIZE];
	memset(full_path, 0, STRING_BUFFER_SIZE);

	char path[255];
	memset(path, 0, 255);

	char playlist[255];
	memset(playlist, 0, 255);
	
	char *audio_files[1024];
	int audio_files_count = 0;

	if (level > MAX_SCAN_DEPTH) {
		return 1;
	}

	level++;
	
	sprintf(path, "%s/%s", MUSIC_DIR, dir);

	dp = opendir (path);

	if (dp != NULL) {
		/* iterate over directory */
		while (ep = readdir (dp)) {
			if((strcmp(ep->d_name,".") != 0) && (strcmp(ep->d_name,"..") != 0)) {
				
				sprintf(full_path, "%s/%s", dir, ep->d_name);
				int retval = scanDirectoryMixed(cue_list, full_path, level, length);

				if(retval == -1) {
					if(regmatch(ep->d_name,"\\.cue") == 0) {
						//parseCUE(dir, full_path, audio, &audio_count);
						memset(playlist, 0, 255);
						strcpy(playlist, full_path);
						//addToList(cue_list, full_path, length);
					}

					if(regmatch(ep->d_name,"\\.m3u") == 0) {
						//addToList(cue_list, full_path, length);
						memset(playlist, 0, 255);
						strcpy(playlist, full_path);
					}

					if(regmatch(ep->d_name,"\\.(flac|mp3|ogg|wav)") == 0) {
						addToList(audio_files, full_path, &audio_files_count);
					}
				} 
			}
		}

		/* if playlist file was found in directory then we add it to global array */
		if(audio_files_count > 0) {
			if(strcmp(playlist, "\0") != 0) {
				TPlaylist *pl = createPlaylist(1, playlist);
				addToStructList(playlists, pl, &playlists_length);
				addToList(cue_list, playlist, length);
			} else {
				TPlaylist *pl = createPlaylist(2, dir);
				addToStructList(playlists, pl, &playlists_length);
			}
		}

		(void) closedir (dp);

		return 0;
	} else {
		return -1;
 	}	
}



int add2Playlist(char *folder) 
{
	char mpdCmd[100];
	memset(mpdCmd, 0, 100);
	int retval = 0;
	//clear playlist
	retval = send2MPD("clear");
	if (retval == 0) {
		//add folder to playlist
		sprintf(mpdCmd, "add \"%s\"", folder);
		printf("%s", mpdCmd);
		retval = send2MPD(mpdCmd);
	}

	return retval;
}

int playSingle(char *fileName, int sec) 
{
	char buff[100] = "\0";

	sprintf(buff,"clear\nrescan\naddid SERV/%s 0\nplay 0", fileName);
	int retval = send2MPD(buff);
	if(sec > 0) {
		sleep(sec);
	}

	return retval;
}

/* 
flag = 1 - INTERNAL
flag = 2 - NAS
flag = 3 - USB
*/
void scanAllFiles(int flag) 
{
	int i;

	clearList(cue, &pl_count);
	clearList(audio, &audio_count);
	clearStructList(playlists, &playlists_length);

	send2MPD("clear");

	char storage_name[MAX_CMD_LENGTH];
	memset(storage_name, 0, MAX_CMD_LENGTH);

	if(flag == 1) {
		sprintf(storage_name, "%s", INTERNAL);
	} else if (flag == 2) {
		sprintf(storage_name, "%s", USB);
	} else {
		printf("Nothing to be done");
		return;
	}

	/* if storage name is not empty string then we scan directories */
	if(strcmp(storage_name, "\0") != 0) {
		scanDirectoryMixed(cue,	storage_name, 0, &pl_count);
	}

	for(i = 0; i < playlists_length; i++) {
		printf("playlist: %s, type: %d\n", playlists[i]->path, playlists[i]->type);
	}
}

void playCurrentList() 
{
	char cmd[MAX_CMD_LENGTH];
	memset(cmd, 0, MAX_CMD_LENGTH);
	
	TPlaylist *current_pl = playlists[current_playlist];

	if(current_pl->type == 1) {
		sprintf(cmd, "clear\nupdate\nload \"%s\"\nplay", current_pl->path);
	} else if (current_pl->type == 2) {
		sprintf(cmd, "clear\nupdate\nadd \"%s\"\nplay", current_pl->path);
	} else {
		return;
	}

	printf("%s\n", cmd);
	int retval = send2MPD(cmd);
}

int executeMpdCmd(char *keycode) 
{
	int k;
	char cmd[MAX_CMD_LENGTH];
	memset(cmd, 0, MAX_CMD_LENGTH);

	for(k = 0; k < 10; k++) {
		char *keynum_tpl = malloc(64*sizeof(char));
		memset(keynum_tpl, 0, 65);
		
		sprintf(keynum_tpl, "KEY_%d", k);
		if(strcmp(keycode, keynum_tpl) == 0) {
			sprintf(keynum_tpl, "play %d", k);
			int retval = send2MPD(keynum_tpl);
		}
	}
	
	/* play CUE only */
	if(strcmp(keycode, "RecTV") == 0) {
		scanAllFiles(1);
		current_playlist = 0;
		playCurrentList();
	}

	/* play CUE only on USB drive */
	if(strcmp(keycode, "Guide") == 0) {
		scanAllFiles(2);
		current_playlist = 0;
		playCurrentList();
	}
	
	if (pl_count > 0) {
		if(strcmp(keycode, "KEY_UP") == 0) {
			if(current_playlist >= playlists_length - 1) {
				current_playlist = 0;
			} else {
				current_playlist++;
			}
			
			playCurrentList();
		}
		
		if(strcmp(keycode,"KEY_DOWN") == 0) {
			if(current_playlist <= 0) {
				current_playlist = playlists_length - 1;
			} else {
				current_playlist--;
			}
			
			playCurrentList();
		}
	}
	
	if(strcmp(keycode,"KEY_CLEAR") == 0) {
		send2MPD("clear");
	}
	
	if(strcmp(keycode,"KEY_PLAY") == 0) {
		send2MPD("play");
	}
	
	if((strcmp(keycode,"KEY_PAUSE") == 0) || (strcmp(keycode,"KEY_MUTE") == 0)) {
		send2MPD("pause");
	}
	
	if(strcmp(keycode,"KEY_RIGHT")==0) {
		send2MPD("next");
	}
	
	if(strcmp(keycode,"KEY_LEFT")==0) {
		send2MPD("previous");
	}
	
	if(strcmp(keycode,"KEY_STOP")==0) {
		send2MPD("stop");
	}
	
	if(strcmp(keycode,"More")==0) {
		send2MPD("playlist");
	}
	
	if(strcmp(keycode,"KEY_FORWARD")==0) {
		send2MPD("seekcur +5");
	}
	
	if(strcmp(keycode,"KEY_REWIND")==0) {
		send2MPD("seekcur -5");
	}
}

// int checkVolumio() 
// {
// 	int x = system("pidof -s node");
// 	printf("pid = %d\n",x);
// 	return x;
// }

int main(int argc, char *argv[])
{
	struct lirc_config *config;
	char *tmp[MAX_LINES];
	long startTime = getMicrotime();
	long endTime = startTime;
	
	char *code;
	char *last_code = "INIT_CODE";
	char *c;
	int ret;
	int pid;

	if(argc > 2) {
		fprintf(stderr,"Usage: %s <config file>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	
	if( lirc_init("irexec", 1) == -1) {
		exit (EXIT_FAILURE);
	}

	if(system("pidof -s mpd") == 0) {
		send2MPD("rescan");
		playSingle("WinBattle.mp3",0);
	} else {
		exit (EXIT_FAILURE);
	}
	
	pid = fork();
	
	if (pid < 0) {
		printf("Error: Start Daemon failed (%s)\n", strerror(errno));
		return -1;
	} else if(pid == 0) {		
		umask(0);
		setsid();
		int r = chdir("/");
		
		close(STDIN_FILENO);
		close(STDOUT_FILENO);
		close(STDERR_FILENO);
		
		//if( lirc_readconfig(argc == 2 ? argv[1] : NULL,&config,NULL) == 0) {	
			printf("Remote controls started successfully!\n");
			while( lirc_nextcode(&code) == 0) {
				
				if ((code == NULL)) {
					continue;
				} 
				
				printf("new code = %s, last code = %s\n", tmp[2], last_code);
				
				int len = 0;
				split(tmp, code, " ", &len);
				char *keycode = strdup(tmp[2]);
					
				if (strcmp(keycode, last_code) == 0) { 
					endTime = getMicrotime();
					int delay = DELAY;
					
					if ( strcmp(keycode, "KEY_FORWARD") == 0 || strcmp(keycode, "KEY_REWIND") == 0 ){
						delay = 50000;
					}
					
					if((endTime - startTime) >= delay){
						free(last_code);
					}

					continue;	
				} else {					
					if(strcmp(keycode, "KEY_POWER") == 0) {
						system("poweroff");
						break;
					}
					
					startTime = getMicrotime();
					
					int retval = executeMpdCmd(keycode);
					last_code = strdup(tmp[2]);
				}

				if((endTime - startTime) > MAX_BUTTON_DELAY) {
					startTime = 0;
					endTime = 0;
				}
				
				free(code);
				if(ret ==- 1) break;
			}

			//lirc_freeconfig(config);
		lirc_deinit();
	}
	printf("End of main program\n");
	return 0;
}