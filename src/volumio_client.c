#include <curl/curl.h>

#define URL "http://%s:3000/api/v1/commands/?cmd=%s"
#define URLN "http://%s:3000/api/v1/commands/?cmd=%s&N=%d"

struct MemoryStruct {
	char *memory;
	size_t size;
};

static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp) {
	size_t realsize = size * nmemb;
	struct MemoryStruct *mem = (struct MemoryStruct *)userp;

	mem->memory = realloc(mem->memory, mem->size + realsize + 1);
	if(mem->memory == NULL) {
	/* out of memory! */ 
	printf("not enough memory (realloc returned NULL)\n");
	return 0;
	}

	memcpy(&(mem->memory[mem->size]), contents, realsize);
	mem->size += realsize;
	mem->memory[mem->size] = 0;

	return realsize;
}

int sendURL(char *url, void *chunk) {
	int flag = 1;
	int httpCode;
	CURL *curl;
	CURLcode res;
	curl_global_init(CURL_GLOBAL_DEFAULT);
	curl = curl_easy_init();
	char tmp[255]; sprintf(tmp,"%s", url);
	//struct MemoryStruct result;
	
	if(curl) {
		//write2log(url);
		curl_easy_setopt(curl, CURLOPT_URL,  url);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
  		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)chunk);
  		curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, 5);
		res = curl_easy_perform(curl);
		/* Check for errors */

		if(res != CURLE_OK) {
			fprintf(stderr, "\ncurl_easy_perform() failed: %s\n", curl_easy_strerror(res));
			flag=0;
			//return 0;
		} else {
    		if(res == CURLE_OK) {
    			curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
    			//printf("response code: %d\n",httpCode);
    		}
		}
		/* always cleanup */
		//curl_easy_cleanup(curl);
	} else {
		//return 0;
		flag=0;
	}
	curl_easy_cleanup(curl);
	curl_global_cleanup();
	
	if(flag==1)
		return httpCode;
	else
		return 0;
}

int volumioSend(char *cmd){
	struct MemoryStruct chunk;
	char url[255];
	memset(url, 0, 255);
	
	chunk.memory = malloc(1); 
  	chunk.size = 0;   
	char *ip = "localhost";

	sprintf(url,URL, ip, cmd);
	
	int http_code = sendURL(url, &chunk);
	if(http_code==200){
		return 0;
	} else {
		return -1;
	}
}

int volumioSendN(char *cmd, int n){
	struct MemoryStruct chunk;
	char url[255];
	memset(url, 0, 255);
	
	chunk.memory = malloc(1); 
  	chunk.size = 0;   
	char *ip = "localhost";
	
	sprintf(url,URLN, ip, cmd, n);
	printf("URL: %s\n", url);
	int http_code = sendURL(url, &chunk);
	if(http_code==200){
		return 0;
	} else {
		return -1;
	}
}