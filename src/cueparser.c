#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <errno.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <regex.h>
#include <sys/time.h>
#include <sys/types.h>

#define MUSIC_DIR "/var/lib/mpd/music"
#define MAX_SCAN_DEPTH 5
#define MAX_MATCH 100

char *cue[255];
char *audio[512];
char *files[512];

int pl_count = 0;
int audio_count = 0;
int files_count = 0;

int current_playlist = 0;


//regmatch
int regmatch(const char *buffer, const char *tpl){
	regex_t regex;
	int reti;

	reti = regcomp(&regex, tpl, REG_EXTENDED);
    if( reti ) { 
    	fprintf(stderr, "Could not compile regex\n"); 
    	return -1;
    }

	reti = regexec(&regex, buffer, 0, NULL, 0);
	if (!reti) {
	    return 0;
	} else {
		return -1;
	}
}

int regmatch_find(char *value, const char *buffer, const char *tpl) {
	regex_t regex;
	regmatch_t m[MAX_MATCH];
	//char *value;
	
	//printf("buffer = %s\n", buffer);
	int reti = regcomp(&regex, tpl, REG_EXTENDED|REG_NEWLINE);
    if( reti ) { 
    	fprintf(stderr, "Could not compile regex\n"); 
    	return -1;
    }

    const char *p = buffer;
    int j=0;
    while (1) {
    	int i = 0;
        int nomatch = regexec(&regex, p, MAX_MATCH, m, 0);
        //printf("\nnomatch = %d", nomatch);
        if (nomatch) {
        	//printf ("No more matches: %d\n", i);
       		return 1;
       	} else {
       		for (i=0; i < MAX_MATCH; i++) {
                int start;
	            int finish;
	            if (m[i].rm_so == -1) {
	                break;
	            }
	            start = m[i].rm_so + (p - buffer);
	            finish = m[i].rm_eo + (p - buffer);

	            if(i > 0) {
	            	int cur_len = (finish - start);
	            	int alloc_mem_size = cur_len + 1;
	            	if(i==1) {
	            		memcpy(value, (buffer + start), cur_len);
	            		return 0;
	            	}
	        	}
            }
            p += m[0].rm_eo;
       		j++; 
       	}
	}
}

void addToList(char **list, char *element, int *length){
	list[*length] = '\0';
	list[*length] = strdup(element);
	*length = *length + 1;
}

void parseCUE(char *directory, char *filename, char **cue_list, int *length) {
 	FILE * fp;

    char line[255];

    size_t len = 0;
    ssize_t read;

	char full_path[512];
	memset(full_path, 0, 512);

    sprintf(full_path, "%s/%s", "/data", filename);
    fp = fopen(full_path, "r");
    if (fp == NULL) {
    	printf("Reading file %s failed\n", full_path);
        exit(EXIT_FAILURE);
    }

    char *line_pointer;
    while (!feof (fp)) {
    	line_pointer = fgets(line, 255, fp);
        //printf("%s", line);
        char value[128] = {'\0'};

        if(regmatch_find(value, line, "FILE \"([^\"]+)\"") == 0) {
        	char full_path[1024] = {'\0'};
			sprintf(full_path, "%s/%s", directory, value);
        	addToList(audio, full_path, &audio_count);
        }
    }

    fclose(fp);
}

int scanDirectoryMixed(char **cue_list, char *dir, int level, int *length) {
	DIR *dp;
	struct dirent *ep;
	char full_path[512];
	memset(full_path, 0, 512);
	
	char path[512];
	memset(full_path, 0, 512);
	
	if (level > MAX_SCAN_DEPTH) {
		return 1;
	}
	level++;
	
	sprintf(path, "%s/%s", MUSIC_DIR, dir);
	dp = opendir (path);
	if (dp != NULL) {
		printf("[%d][%d]Directory: %s\n",level, *length, dir);
		while (ep = readdir (dp)) {
			if((strcmp(ep->d_name,".") != 0) && (strcmp(ep->d_name,"..") != 0)) {
				sprintf(full_path, "%s/%s", dir, ep->d_name);
				int retval = scanDirectoryMixed(cue_list, full_path, level, length);
				//printf("RETVAL=%d\n", retval);
				if(retval==-1) {
					if(regmatch(ep->d_name,"\\.cue") == 0) {
						//printf("[%d][%d]CUE file: %s\n", level, *length, full_path);
						parseCUE(dir, full_path, cue_list, length);
						addToList(cue_list, full_path, length);
					}

					if(regmatch(ep->d_name,"\\.(flac|mp3|ogg|wav)") == 0) {
						//printf("[%d][%d]AUDIO file: %s\n", level, *length, full_path);
						addToList(files, full_path, &files_count);
					}
				} 
			}
		}
		(void) closedir (dp);
		return 0;
	} else {
 		//sprintf(error_msg, "Couldn't open the directory %s", dir);
		return -1;
 	}
	
}

int in_array(char *element, char **arr, int length) {
	int i=0;
	for(i=0; i < length; i++) {
		if(strcmp(element, arr[i]) == 0){
			return 0;
		}
	}
	return 1;
}

void print_r(char **arr, int length){
	int k=0;
	for(k=0; k < length; k++) {
		printf("\"%s\" \n", arr[k]);
		//sprintf(cmd,"load \"%s\"", cue[k]);
		//int retval = send2MPD(cmd);
	}
}

int main(int argc, char *argv[]) {
	int i=0;
	char *result[1024];

	scanDirectoryMixed(cue,"INTERNAL",0, &pl_count);
	

	int result_length;

	// for(i = 0; i < pl_count; i++) {
	// 	addToList(result, cue[i], &result_length);
	// }

	for(i = 0; i < files_count; i++) {
		if(in_array(files[i], audio, audio_count) == 1){
			addToList(result, files[i], &result_length);
		}
	}

	printf("====CUE==========================\n");

	print_r(cue, pl_count);

	printf("====FILES==========================\n");
	print_r(result, result_length);
}